# SleepWell
SleepWell is a simple sleep tracking app. SleepWell try its best to keep up to date with your sleeping data
and provide score to your sleep quality. SleepWell also try to help you to have better sleep via releasing
your daily stress with breath exercise.

**Note:** This is a prototype project which build for the assignment of the course TCS3254 Programming for
Mobile Devices

## Build
This project is build with [Node.js](https://www.nodejs.org) and [React Native](https://reactnative.dev/) with
[Expo](https://expo.io), ensure to have the **latest LTS version** of Node.js installed.

Install Expo cli
```
npm install expo-cli -g
```

Install required libs and start project
```
npm start
```

Scan the displayed QR code with Expo app on Android or iOS camera, require Expo app installed

Check [Expo documentation](https://docs.expo.io/distribution/building-standalone-apps/?redirected) for
building standalone app.