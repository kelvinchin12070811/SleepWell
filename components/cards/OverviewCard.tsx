import { FontAwesome5 } from '@expo/vector-icons';
import React from 'react';
import { TouchableOpacity, StyleSheet, View } from 'react-native';

import { AddItem } from '../../screens/AddItem';
import { Card } from './Card';
import { ThemedText } from '../ThemedText';
import { gstyles } from '../../styles/Global';
import { scheme } from '../../styles/Scheme';
import SleepDuration from '../../legacy/SleepDuration';
import { useNavigation } from '@react-navigation/native';

/** Props of overview card */
interface OverviewCardProps
{
    today: Date; /**< Today's date */
    lastSleptStat: SleepTimePoint; /**< Last sleep data. */
    onAddData: (data: SleepDuration) => void;
}

/**
 * Component that give overview of user last sleep.
 * Take 3 required props, see OverviewCardProps for reference
 */
export const OverviewCard: React.FC<OverviewCardProps> = ({ today, lastSleptStat, onAddData }) => {
    const navigation = useNavigation();
    const openForm = () => {
        navigation.navigate("AddItem", { setItem: onAddData });
    };

    return (
        <Card style={ gstyles.cards }>
        { (() => {
            return (
                <View>
                    <ThemedText style={{ ...gstyles.cardsText, ...gstyles.weekdayTitle }}>
                    {(() => {
                        switch(today.getDay())
                        {
                        case 1:
                            return "Monday";
                        case 2:
                            return "Tuesday";
                        case 3:
                            return "Wednesday";
                        case 4:
                            return "Thursday";
                        case 5:
                            return "Friday";
                        case 6:
                            return "Saturday";
                        default:
                            return "Sunday";
                        }
                    })()}
                    </ThemedText>
                    <ThemedText style={ gstyles.cardsText }>
                    {(() => {
                        let monthName = "";
                        
                        switch (today.getMonth())
                        {
                        case 1:
                            monthName = "February";
                            break;
                        case 2:
                            monthName = "March";
                            break;
                        case 3:
                            monthName = "Apirl";
                            break;
                        case 4:
                            monthName = "May";
                            break;
                        case 5:
                            monthName = "June";
                            break;
                        case 6:
                            monthName = "July";
                            break;
                        case 7:
                            monthName = "August";
                            break;
                        case 8:
                            monthName = "September";
                            break;
                        case 9:
                            monthName = "October";
                            break;
                        case 10:
                            monthName = "Noverber";
                            break;
                        case 11:
                            monthName = "December";
                            break;
                        default:
                            monthName = "January";
                            break;
                        }

                        return `${ today.getDate() } ${ monthName } ${ today.getFullYear() }`;
                    })()}
                    </ThemedText>
                    <View
                        style={{
                            marginTop: 30,
                            marginBottom: 0,
                            flexDirection: "row",
                            justifyContent: "center",
                            alignItems: "center"
                        }}
                    >
                        <ThemedText style={ gstyles.lastSleepTime }>
                            { lastSleptStat.hour }
                        </ThemedText>
                        <ThemedText> Hours </ThemedText>
                        <ThemedText style={ gstyles.lastSleepTime }>
                            { lastSleptStat.minute }
                        </ThemedText>
                        <ThemedText> Minutes</ThemedText>
                    </View>
                    <ThemedText style={{
                        marginTop: -10,
                        marginBottom: 30,
                        textAlign: "center"
                    }}>
                        Total sleep time
                    </ThemedText>
                    <ThemedText style={ gstyles.cardsText }>
                        Try keep regular sleep time and wake time!
                    </ThemedText>
                </View>
            )
        })()}
        <TouchableOpacity
            style={ styles.button }
            onPress={ openForm }
        >
            <FontAwesome5
                name="plus"
                size={ 20 }
                style={{
                    marginRight: 5,
                    color: scheme.theme.l2.color
                }}
            />
            <ThemedText>Add Data</ThemedText>
        </TouchableOpacity>
    </Card>
    );
};

/**
 * Styles of OverviewCard
 */
const styles = StyleSheet.create({
    /** Button style */
    button: {
        backgroundColor: scheme.theme.l2.bgcolor,
        width: "100%",
        paddingVertical: 10,
        marginTop: 20,
        borderRadius: 20,
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center"
    }
});