import React from 'react';
import { View, StyleSheet } from 'react-native';

import { ThemedText } from '../ThemedText';

import { gstyles } from '../../styles/Global';
import { scheme } from '../../styles/Scheme';

interface CardProps
{
    style?: any; /**< Addidional stylesheet to style elements wrapped with Card. */
}

/**
 * @brief Card component is the section of each presentation board. It used by SleepWell to display data to
 * user. The content need to present with Card should be wrapped as the child of Card.
 */
export const Card: React.FC<CardProps> = ({ children, style }) => {
    return (
        <View style={{ ...styles.container, ...style }}>
            { children }
        </View>
    );
}

/** Internal styles used by Card */
const styles = StyleSheet.create({
    /** Style for main container of Card */
    container: {
        borderRadius: 10,
        backgroundColor: scheme.theme.l3.bgcolor,
        paddingVertical: 16,
        paddingHorizontal: 20
    }
});