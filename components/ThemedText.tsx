import React from 'react';
import { Text, StyleSheet } from 'react-native';

import { gstyles } from '../styles/Global';

/**
 * @brief Type of the props of ThemedTextProps
 */
interface ThemedTextProps
{
    style?: any; /**< Optional additional styles that use to style ThemedText. */
}

/**
 * ThemedText is the addon to react native build in TextComponent, it has default style to match the dark
 * background of SleepWell.
 * @param param0 Properties that passed into ThemedText, see ThemedTextProps for more details.
 */
export const ThemedText: React.FC<ThemedTextProps> = ({ children, style }) => {
    return <Text style={{ ...gstyles.text, ...style }}>{ children }</Text>;
};