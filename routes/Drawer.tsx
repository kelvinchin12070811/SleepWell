import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import React from 'react';
import { StatusBar } from 'react-native';

import { AboutStack } from './AboutStack';
import * as Header from '../components/Header';
import { TipsStack } from './TipsStack';
import { HomeStack } from './HomeStack';
import { scheme } from '../styles/Scheme';

/**
 * Factory function of the react-navigation/drawer
 */
const Navigation = createDrawerNavigator();

/**
 * @brief App drawer of SleepWell app
 */
export const Drawer: React.FC = () => {
    StatusBar.setBackgroundColor(scheme.theme.l1.bgcolor);
    return (
        <NavigationContainer>
            <Navigation.Navigator
                initialRouteName="Dashbord"
                drawerStyle={ Header.getHeaderPlaceholderStyle() }
                drawerContentOptions={ Header.getHeaderOption() }
            >
                <Navigation.Screen name="Dashboard" component={ HomeStack }/>
                <Navigation.Screen name="Tips" component={ TipsStack }/>
                <Navigation.Screen name="About" component={ AboutStack}/>
            </Navigation.Navigator>
        </NavigationContainer>
    );
};