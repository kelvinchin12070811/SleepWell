import React from 'react';
import { createStackNavigator, HeaderBackground, StackNavigationProp } from '@react-navigation/stack';

import { AddItem } from '../screens/AddItem';
import { Home } from '../screens/Home';
import { DetailList } from '../screens/DetailList';
import * as Header from '../components/Header';
import { scheme } from '../styles/Scheme';

import SleepDuration from '../legacy/SleepDuration';

/**
 * Factory function of react-navigation/stack
 */
const Stack = createStackNavigator();

/**
 * @brief Stack Navigation for Home stack
 */
export const HomeStack: React.FC = () => {
    return (
        <Stack.Navigator initialRouteName="Home" screenOptions={ Header.getHeaderStyle() }>
            <Stack.Screen
                name="Home"
                component={ Home }
                options={ props => Header.getHeader(props, "Dashboard") }
            />
            <Stack.Screen
                name="DetailList"
                component={ DetailList }
                options={{
                    headerTintColor:scheme.theme.l3.color,
                    title: "Sleep Log"
                }}
            />
            <Stack.Screen
                name="AddItem"
                component={ AddItem }
                options={{
                    headerTintColor:scheme.theme.l3.color,
                    title: "Add New Sleep Record"
                }}
            />
        </Stack.Navigator>
    );
};