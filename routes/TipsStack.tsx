import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import { Tips } from '../screens/Tips';
import * as Header from '../components/Header';

/**
 * Factory function of react-navigation/stack
 */
const Stack = createStackNavigator();

/**
 * @brief StackNavigation of About page
 */
export const TipsStack: React.FC = () => {
    return (
        <Stack.Navigator initialRouteName="Tips" screenOptions={ Header.getHeaderStyle() }>
            <Stack.Screen
                name="Tips"
                component={ Tips }
                options={ props => Header.getHeader(props, "Tips") }
            />
        </Stack.Navigator>
    );
};