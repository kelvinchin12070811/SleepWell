import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import { About } from '../screens/About';
import * as Header from '../components/Header';

/**
 * Factory function of react-navigation/stack
 */
const Stack = createStackNavigator();

/**
 * @brief StackNavigation of About page
 */
export const AboutStack: React.FC = () => {
    return (
        <Stack.Navigator initialRouteName="About" screenOptions={ Header.getHeaderStyle() }>
            <Stack.Screen
                name="About"
                component={ About }
                options={ props => Header.getHeader(props, "About") }
            />
        </Stack.Navigator>
    );
};