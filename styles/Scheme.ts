/*************************************************************************************************************
* Colour scheme for SleepWell, values generated with W3.CSS Color Generator by w3schools.com
*************************************************************************************************************/

/**
 * Default theme colors
 */
export const scheme: schemes.ThemeScheme = {
    theme: {
        l5: {
            color: "#000",
            bgcolor: "#ede4ff"
        },
        l4: {
            color: "#000",
            bgcolor: "#c2a6ff"
        },
        l3: {
            color: "#fff",
            bgcolor: "#864eff"
        },
        l2: {
            color: "#fff",
            bgcolor: "#4d00f4"
        },
        l1: {
            color: "#fff",
            bgcolor: "#31009b"
        },
        d1: {
            color: "#fff",
            bgcolor: "#13003c"
        },
        d2: {
            color: "#fff",
            bgcolor: "#110035"
        },
        d3: {
            color: "#fff",
            bgcolor: "#0f002e"
        },
        d4: {
            color: "#fff",
            bgcolor: "#0d0028"
        },
        d5: {
            color: "#fff",
            bgcolor: "#0a0021"
        },
        light: {
            color: "#000",
            bgcolor: "#ede4ff"
        },
        dark: {
            color: "#fff",
            bgcolor: "#0a0021"
        },
        action: {
            color: "#fff",
            bgcolor: "#0a0021"
        },
        default: {
            color: "#fff",
            bgcolor: "#140040"
        },
        text: {
            color: "#140040",
            bgcolor: "#140040"
        },
        border: {
            color: "#140040",
            bgcolor: "#140040"
        },
        hoverTheme: {
            color: "#fff",
            bgcolor: "#140040"
        },
        hoverText: {
            color: "#140040",
            bgcolor: "#140040"
        },
        hoverBorder: {
            color: "#140040",
            bgcolor: "#140040"
        }
    }
};