/** Schemes section */
namespace schemes
{
    /** Type of themes scheme */
    interface ThemeScheme
    {
        theme: Themes;
    }

    /** List of themes */
    interface Themes
    {
        l5: Theme;
        l4: Theme;
        l3: Theme;
        l2: Theme;
        l1: Theme;
        d1: Theme;
        d2: Theme;
        d3: Theme;
        d4: Theme;
        d5: Theme;
        light: Theme;
        dark: Theme;
        action: Theme;
        default: Theme;
        text: Theme;
        border: Theme;
        hoverTheme: Theme;
        hoverText: Theme;
        hoverBorder: Theme;
    }

    /** Type of theme */
    interface Theme
    {
        color: string; /**< Foreground color */
        bgcolor: string; /**< Background color */
    }
}

/** Time type */
type SleepTimePoint = {
    hour: number; /**< Hours of time. */
    minute: number; /**< Minute of time. */
}