/**
 * @brief SleepDuration is the object that record the duration of sleep time.
 */
class SleepDuration
{
    /** [Read & Write] Time when user start to sleep. */
    private sleptTime: SleepTimePoint;
    /** [Read & Write] Time when user awake. */
    private awakeTime: SleepTimePoint;
    /** [Read & Write] Date of the record, formated in ISO 8601. */
    private date: string;
    /** [Read & Write] Rating of user on their sleep. */
    private rating: number;

    /**
     * Construct new instance of SleepDuration.
     * @param sleptTime Time when user slept
     * @param awakeTime Time when user awake
     * @param date Date of the record
     * @param rating How user feel to their sleep, (1-5 hearts)
     */
    public constructor(sleptTime: SleepTimePoint, awakeTime: SleepTimePoint, date: string, rating: number)
    {
        this.awakeTime = awakeTime;
        this.sleptTime = sleptTime;
        this.date = date;
        this.rating = rating;
    }

    /**
     * Get the duration of total slept hour
     */
    public getDuration(): SleepTimePoint
    {
        let result: SleepTimePoint = { hour: 0, minute: 0 };
        result.hour = 24 - (this.sleptTime.hour + (this.sleptTime.minute / 60));
        result.hour += this.awakeTime.hour + (this.awakeTime.minute / 60.0);
        result.minute = Math.floor((result.hour - Math.floor(result.hour)) * 60);
        result.hour = Math.floor(result.hour);
        return result;
    }

    // Getters
    public getSleptTime(): SleepTimePoint
    {
        return this.sleptTime;
    }

    public getAwakeTime(): SleepTimePoint
    {
        return this.awakeTime;
    }

    public getDate(): string
    {
        return this.date;
    }

    public getRating(): number
    {
        return this.rating;
    }

    // Setters
    public setSleptTime(value: SleepTimePoint)
    {
        this.sleptTime = value;
    }

    public setAwakeTime(value: SleepTimePoint)
    {
        this.awakeTime = value;
    }

    public setDate(value: string)
    {
        this.date = value;
    }

    public setRating(value: number)
    {
        this.rating = value;
    }
}

export default SleepDuration;