import React, { useState, useEffect } from 'react';
import { View, StyleSheet } from 'react-native';
import { ThemedText } from '../components/ThemedText';

import { gstyles } from '../styles/Global';

function useForceUpdate()
{
    const [value, setValue] = useState(0); // integer state
    return () => setValue(value => ++value); // update the state to force render
}

export const Tips: React.FC = () => {
    const tips = [
        "Release stress is able to help with sleep.",
        "Some people might lisenting music before sleep, but is better to clear out your mind.",
        "Taking a cup of milk is best before sleep.",
        "No milk? No worry, a cup of warm water have the same effect.",
        "No idea how to sleep? Take a look at the Buku Teks Sejarah! (stop, not funny)."
    ];

    const [idx, setIdx] = useState(0);

    useEffect(() => {
        setIdx(Math.floor(Math.random() * ((tips.length - 1) - 0) + 1));
    });

    const forceUpdate = useForceUpdate();
    return (
        <View style={{ ...gstyles.container, ...styles.container }}>
            <ThemedText style={{ fontWeight: "bold", fontSize:25, marginBottom: 10 }}>Do you know?</ThemedText>
            <ThemedText style={{ textAlign: "center" }}>{ (() => tips[Math.floor(idx)])() }</ThemedText>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        justifyContent: "center",
        alignItems: "center"
    }
});