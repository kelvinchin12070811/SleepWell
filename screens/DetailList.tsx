import { FontAwesome, MaterialCommunityIcons } from '@expo/vector-icons';
import { useRoute } from '@react-navigation/native';
import React from 'react';
import { View, StyleSheet, ScrollView } from 'react-native';

import { Card } from '../components/cards/Card';
import { gstyles } from '../styles/Global';
import { ThemedText } from '../components/ThemedText';
import SleepDuration from '../legacy/SleepDuration';

/**
 * @brief The component that list all recorede details of user's sleep data.
 * Refer DetailListProps for required properties.
 */
export const DetailList: React.FC = () => {
    const route = useRoute<any>();
    
    const getDayName = (day: number) => {
        switch (day)
        {
        case 1:
            return "Monday";
        case 2:
            return "Tuesday";
        case 3:
            return "Wednesday";
        case 4:
            return "Thursday";
        case 5:
            return "Friday";
        case 6:
            return "Saturday";
        default:
            return "Sunday";
        }
    };

    const getMonthName = (month: number) => {
        switch(month)
        {
        case 1:
            return "February";
        case 2:
            return "March";
        case 3:
            return "Apirl";
        case 4:
            return "May";
        case 5:
            return "June";
        case 6:
            return "July";
        case 7:
            return "August";
        case 8:
            return "September";
        case 9:
            return "October";
        case 10:
            return "Noverber";
        case 11:
            return "December";
        default:
            return "January";
        }
    };

    return(
        <View style={{ ...gstyles.container, paddingVertical: (16-5), paddingHorizontal: 16 }}>
        <ScrollView>
        {
            route.params?.sleepData.map((itr: SleepDuration, idx: number) => {
                const date = new Date(itr.getDate())
                return (
                    <Card
                        style={{
                            marginVertical: 5
                        }}
                        key={ idx }
                    >
                        <ThemedText style={{ ...styles.cardText, ...styles.cardTitle }}>
                            { `${ date.getDate() } ${ getMonthName(date.getMonth()) } ${ date.getFullYear() }` }
                        </ThemedText>
                        <ThemedText style={ styles.cardText }>
                            { getDayName(date.getDay()) }
                        </ThemedText>
                        <View style={ styles.sleepDuration }>
                            <View style={ styles.sleepCard }>
                                <MaterialCommunityIcons
                                    style={ styles.cardLabel }
                                    name="weather-night"
                                    size={ 40 }
                                />
                                <ThemedText>
                                { 
                                    (() => {
                                        const sleptTime = itr.getSleptTime();
                                        return (
                                            `${sleptTime.hour > 12 ? sleptTime.hour - 12 : sleptTime.hour}:` +
                                            `${sleptTime.minute < 10 ? '0' : ''}${sleptTime.minute}` +
                                            `${sleptTime.hour >= 12 ? 'PM' : 'AM'}`
                                        );
                                    })()
                                }
                                </ThemedText>
                            </View>
                            <View style={ styles.sleepCard }>
                                <MaterialCommunityIcons
                                    style={ styles.cardLabel }
                                    name="weather-sunset"
                                    size={ 40 }
                                />
                                <ThemedText>
                                { 
                                    (() => {
                                        const sleptTime = itr.getAwakeTime();
                                        return (
                                            `${sleptTime.hour > 12 ? sleptTime.hour - 12 : sleptTime.hour}:` +
                                            `${sleptTime.minute < 10 ? '0' : ''}${sleptTime.minute}` +
                                            `${sleptTime.hour >= 12 ? 'PM' : 'AM'}`
                                        );
                                    })()
                                }
                                </ThemedText>
                            </View>
                        </View>
                        <View style={{ flexDirection: "row", justifyContent: 'center', marginTop: 12}}>
                        {
                            (() => {
                                let stars = [];
                                for (let x = 0; x < itr.getRating(); x++)
                                    stars.push(<FontAwesome key={x} style={{color: 'white'}} size={20} name="star"/>);
                                return stars;
                            })()
                        }
                        </View>
                    </Card>
                );
            })
        }
        </ScrollView>
        </View>
    );
};

const styles = StyleSheet.create({
    cardText: {
        textAlign: "center"
    },
    cardTitle: {
        fontSize: 20,
        fontWeight: "bold"
    },
    cardLabel: {
        color: "white"
    },
    sleepDuration: {
        flexDirection: "row",
        justifyContent: "space-around"
    },
    sleepCard: {
        alignItems: "center"
    }
});